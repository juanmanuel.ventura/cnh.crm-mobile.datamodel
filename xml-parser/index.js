/**
 * Created by matteodellea on 15/03/16.
 */

"use strict";

const sax = require("sax");

const isFunction = function(fn) {
  return Object.prototype.toString.call(fn) === "[object Function]";
};

const castToBoolean = function(v) {
  return !(v === "0" || v === "false")
};

const castToSchemaType = function(obj, schema, key, value) {
  if(!schema[key] || !isFunction(schema[key])) return obj[key] = value;
  return obj[key] = schema[key].name === "Boolean" ? castToBoolean(value) : schema[key](value);
};

const sanitizeTag = function(result, tagObj, schema) {
  schema = schema || {};
  Object.keys(tagObj.attributes).forEach(function(k) {
    var key = k.toLowerCase();
    castToSchemaType(result, schema, key, tagObj.attributes[k]);
  });
  return result;
};

/**
 * Parses a xml using the schema
 * @param xml {String}
 * @param [schema {Object}] object representing the object structure of the xml
 * @param [nodeNameMapping {object}] object mapping the nodeName with the appropriate node name
 * @param callback {Function} simple callback
 */
const parser = function(xml) {
  var schema = arguments.length > 2 ? arguments[1] : {};
  var nodeNameMapping = arguments.length > 3 ? arguments[2] : {};
  var callback = arguments[arguments.length-1];
  var parser = sax.parser(true);
  var result = {};
  var pointers = [result];
  var schemaPointers = [schema];

  parser.onopentag = function (node) {
    var parent = pointers[pointers.length-1];
    var schemaParent = schemaPointers[schemaPointers.length-1];
    var key = node.name.toLowerCase();
    key = nodeNameMapping[key] ? nodeNameMapping[key] : key;
    var arrayChildren = parent[key] = parent[key] || [];
    var currentSchema = schemaParent && schemaParent[key] && schemaParent[key][0] ? schemaParent[key][0] : {};
    var obj = {};
    arrayChildren.push(obj);
    sanitizeTag(obj, node, currentSchema);
    pointers.push(obj);
    schemaPointers.push(currentSchema);
  };
  parser.onclosetag = function() {
    pointers.pop();
    schemaPointers.pop();
  };
  parser.onerror = function(err) {
    callback(err, {});
  };
  parser.onend = function () {
    callback(null, result);
  };

  parser.write(xml).close();
};

module.exports = parser;