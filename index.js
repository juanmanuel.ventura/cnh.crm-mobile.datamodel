/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

const async = require("async");
const schemas = require("./schema");
const configs = require("./config");
const xmlParser = require("./xml-parser");
const typeArray = "[object Array]",
  typeObject = "[object Object]",
  typeOf = o => Object.prototype.toString.call(o);

const typeConverter = function (data, config) {
  const type = typeOf(data);
  if (type === typeObject) return Object.keys(data).forEach(function (key) {
    let type = typeOf(data[key]);
    if (data[key] == null) return;
    if (type === typeArray) return data[key].forEach(d => typeConverter(d, config[key][0]));
    if (type === typeObject) return typeConverter(data[key], config[key]);
    if (config[key]) data[key] = config[key](data[key]);
    else console.log("key:", key, "not found!");
  });
  if (type === typeArray) return data.forEach(d => typeConverter(d, config[0]));
  data = config(data);
};

const dataGenerator = function (lineArray, config, schema, callback) {
  var obj = {};
  const iterator = function (fieldValue, i, cb) {
    let field = config.fields[i];
    if (fieldValue == null || fieldValue == "NULL" || !field) return cb(null, true);
    if (field.type === "xml") {
      let key = field.name.toLowerCase();
      xmlParser(fieldValue, schema, field.mapping, function (err, ris) {
        obj[key] = ris[key];
        cb(err, true);
      });
    } else {
      let key = field.toLowerCase();
      if (schema[key]) obj[key] = schema[key](fieldValue);
      cb(null, true);
    }
  };
  async.parallel(
    lineArray.map((line, i) => iterator.bind(null, line, i)),
    (err, ris) => callback(err, obj)
  );
};

module.exports = {
  /**
   * Casts the object passed to match the schema defined for the passed type
   * @param data {Object} the object to "cast"
   * @param type {String} the entity name
   */
  typeConverter: function (data, type) {
    if (!type || !schemas[type]) return console.error("NO SCHEMA FOUND FOR THAT TYPE: ", type);
    return typeConverter(data, schemas[type]);
  },

  /**
   * Generate an object structured as the schema of the 'entityName' says, also parses the xml if the field is
   * @param lineArray {Array} the array to parse
   * @param entityName {String} the entity name
   * @param cb {Function}
   */
  dataGenerator: function (lineArray, entityName, cb) {
    if (!entityName || !schemas[entityName]) return cb(`NO SCHEMA FOUND FOR THAT TYPE: ${entityName}`, {});
    dataGenerator(lineArray, configs[entityName], schemas[entityName], function (err, d) {
      if(err) return cb(err, d);
      d.type = entityName;
      if (configs[entityName].cleanup) configs[entityName].cleanup(d);
      cb(err, d);
    });
  }
};