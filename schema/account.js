/**
 * Created by matteodellea on 11/03/16.
 */

"use strict";

const Assignment = require('./secondary/assignment');
const BrandStatistic = require('./secondary/brand-statistic');
const CustomerAddress = require('./secondary/customer-address');
const CustomerStatistic = require('./secondary/customer-statistic');

module.exports = {
  accountcategorycode: Number,
  accountid: String,
  address1_addresstypecode: Number,
  address1_city: String,
  address1_country: String,
  address1_latitude: Number,
  address1_line1: String,
  address1_longitude: Number,
  address1_postalcode: String,
  address1_primarycontactname: String,
  address1_stateorprovince: String,
  address1_telephone1: String,
  address1_telephone2: String,
  address1_telephone3: String,
  assignments: [Assignment],
  brandstatistics: [BrandStatistic],
  customeraddress: [CustomerAddress],
  customerstatistic: [CustomerStatistic],
  description: String,
  donotbulkemail: Boolean,
  donotemail: Boolean,
  donotphone: Boolean,
  donotpostalmail: Boolean,
  donotsendmm: Boolean,
  emailaddress1: String,
  ivc_assignmentresearchkeys: String,
  ivc_isblocked: Boolean,
  masteraccountidname: String,
  masterid: String,
  name: String,
  new_classeid: String,
  new_contact: Boolean,
  new_frazione: String,
  new_gruppoid: String,
  new_lastcontactdate: String,
  new_nextcontactdate: String,
  new_ordered_name_account: String,
  new_partitaiva: String,
  new_preferredworkshopid: String,
  new_sezioneid: String,
  new_statonormalizzazione: Number,
  new_taxcode: String,
  primarycontactid: String,
  primarycontactidname: String,
  statecode: Number,
  websiteurl: String
};