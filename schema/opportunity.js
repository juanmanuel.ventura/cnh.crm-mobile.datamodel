/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

const QuoteProduct = require("./secondary/quote-product");

module.exports = {
  actualclosedate: String,
  actualvalue: String,
  campaignid: String,
  createdon: String,
  customerid: String,
  modifiedon: String,
  name: String,
  new_competitorid: String,
  new_opendate: String,
  new_probability: String,
  new_referencecontactsid: String,
  new_sourcecontact: Number,
  opportunityid: String,
  ownerid: String,
  owneridname: String,
  quoteproduct: [QuoteProduct],
  statecode: Number,
  statuscode: Number
};