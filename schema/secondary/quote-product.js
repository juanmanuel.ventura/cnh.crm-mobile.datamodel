/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

const Leasing = require("./leasing");
const Body = require("./body");
const IvecoBody = require("./iveco-body");
const Trade = require("./trade");

module.exports = {
  createdon: String,
  ivc_preferredproduct: Boolean,
  ivc_salesactionid: String,
  ivc_sequencenumber: String,
  modifiedon: String,
  new_brandid: String,
  new_brandidname: String,
  new_commercialsectorid: String,
  new_commissions_np: String,
  new_commissions_np_m: String,
  new_dealerbodies_np: String,
  new_dealerbodies_np_m: String,
  new_ivecobodies_new_np: String,
  new_ivecobodies_np: String,
  new_ivecobodiesbase_new_np: String,
  new_ivecobodiesbase_np: String,
  new_ivecobody: [IvecoBody],
  new_manualbody: String,
  new_manualmodel: String,
  new_margin: Number,
  new_marginvalue: String,
  new_newusedid: String,
  new_opportunityid: String,
  new_pdi_np: String,
  new_pdi_np_m: String,
  new_producttype: Number,
  new_quantity: Number,
  new_quoteproductid: String,
  new_registration_np: String,
  new_registration_np_m: String,
  new_registrationdocument_np: String,
  new_registrationdocument_np_m: String,
  new_total_np: String,
  new_totalproduct_dp: Number,
  new_totalproduct_dp_m: Number,
  new_totalproduct_dv: String,
  new_totalproduct_dv_m: String,
  new_totalproduct_gp_m: String,
  new_tradein_np: String,
  new_transport_np: String,
  new_transport_np_m: String,
  newbody: [Body],
  newleasing: [Leasing],
  newtrade: [Trade],
  ownerid: String,
  statecode: Number
};