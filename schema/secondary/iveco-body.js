/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  createdon: String,
  ivc_type: Number,
  modifiedon: String,
  new_alternative: Boolean,
  new_bodybuilderid: String,
  new_bodycategoryid: String,
  new_ccm: String,
  new_customerdiscountpercent: Number,
  new_customerdiscountvalue: String,
  new_customernetprice: String,
  new_dealerdiscountpercent: Number,
  new_dealerdiscountvalue: String,
  new_dealernetprice: String,
  new_ivecobodyid: String,
  new_longdescription: String,
  new_pricelistprice: String,
  new_range: Number,
  new_remarks: String,
  new_shortdescription: String,
  ownerid: String
};