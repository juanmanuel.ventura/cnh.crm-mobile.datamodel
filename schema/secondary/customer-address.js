/**
 * Created by matteodellea on 11/03/16.
 */

"use strict";

module.exports = {
  "addressnumber": Number,
  "city": String,
  "country": String,
  "customeraddressid": String,
  "latitude": Number,
  "line1": String,
  "longitude": Number,
  "new_normalizationstate": Number,
  "parent": String,
  "parentid": String,
  "parentidtypecode": Number,
  "postalcode": String,
  "stateorprovince": String,
  "tag": Number
}