/**
 * Created by matteodellea on 11/03/16.
 */

"use strict";

module.exports = {
  "ivc_accountid": String,
  "ivc_accountidname": String,
  "ivc_hc": Boolean,
  "ivc_hsc": Boolean,
  "ivc_hst": Boolean,
  "ivc_l1c": Boolean,
  "ivc_l1f": Boolean,
  "ivc_l2c": Boolean,
  "ivc_l2f": Boolean,
  "ivc_l3": Boolean,
  "ivc_m1": Boolean,
  "ivc_m2": Boolean,
  "ivc_salesmanid": String,
  "ivc_salesmanidname": String,
  "ownerid": String,
  "owneridname": String,
  "statecode": Number
};