/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  createdon: String,                      //details_
  ivc_bodycategoryid: String,             //details_
  ivc_brandid: String,                    //details_
  ivc_chassis: String,                    //details_
  ivc_firstregistrationdate: String,      //details_
  ivc_km: Number,                         //details_
  ivc_manualmodel: String,                //details_
  ivc_marketprice: String,                //details_
  ivc_modelid: String,                    //details_
  ivc_plate: String,                      //details_
  ivc_quantity: Number,
  ivc_tradeid: String,                    //details_
  ivc_tradeindetailsid: String,
  ivc_unitarymarketprice: String,
  ivc_unitaryprovision: String,
  ivc_unitarywithdrawalprice: String,
  ivc_weightclassid: String,              //details_
  ivc_withdrawalplanneddate: String,      //details_
  ivc_withdrawalprice: String,            //details_
  modifiedon: String,                     //details_
  new_tradeid: String
};