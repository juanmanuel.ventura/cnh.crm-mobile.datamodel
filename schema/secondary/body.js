/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  createdon: String,
  modifiedon: String,
  new_accessory: Boolean,
  new_alternative: Boolean,
  new_bodycategoryid: String,
  new_bodydescription: String,
  new_customerdiscountpercent: Number,
  new_grosspricecustomer: String,
  new_name: String,
  new_netpricecustomer: String,
  new_netpricedealer: String
};