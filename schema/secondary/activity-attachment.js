/**
 * Created by Gabriele on 25/03/16.
 */

"use strict";

module.exports = {
    "activityid": String ,
    "filename": String ,
    "mimetype": String ,
    "body": String
};