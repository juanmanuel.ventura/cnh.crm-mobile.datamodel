/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  createdon: String,
  ivc_costofopeningperc: Number,
  ivc_favourite: Boolean,
  ivc_financingobject: Number,
  ivc_leasingcampaignid: String,
  ivc_proposalnumber: String,
  ivc_residualvaluepercent: Number,
  ivc_typeofrate: Number,
  modifiedon: String,
  new_commissionpercent: Number,
  new_commissionvalue: String,
  new_costofopening: String,
  new_depositguarantee: String,
  new_depositpercent: Number,
  new_depositvalue: String,
  new_installmentamount: String,
  new_installments: Number,
  new_interest: Number,
  new_km: Number,
  new_leasingid: String,
  new_nominalrate: Number,
  new_payment: Number,
  new_residualvalue: String,
  new_subsidy: String,
  new_totalnetprice: String,
  new_type: Number,
  new_valuetofund: String
};