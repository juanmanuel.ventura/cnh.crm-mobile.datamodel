/**
 * Created by matteodellea on 11/03/16.
 */

"use strict";

module.exports = {
  ivc_firstpurchasedate: String,
  ivc_lastpurchasebrandname: String,
  ivc_lastpurchasedate: String,
  new_indicedifedelta: Number,
  new_tempomediodiriacquisto: Number,
  new_tot: Number
};