/**
 * Created by matteodellea on 11/03/16.
 */

"use strict";

module.exports = {
  "new_brandid": String,
  "new_brandidname": String,
  "new_brandstatisticid": String,
  "new_omogeneo": Number,
  "new_tot": Number,
  "new_totl": Number,
  "new_totl1": Number,
  "new_totl1c": Number,
  "new_totl1f": Number,
  "new_totl2": Number,
  "new_totl2c": Number,
  "new_totl2f": Number,
  "new_totl3": Number,
  "new_totm": Number,
  "new_totm1": Number,
  "new_totm2": Number,
  "new_totnuovi": Number,
  "new_totp": Number,
  "new_totpc": Number,
  "new_totps": Number,
  "new_totpsc": Number,
  "new_totpst": Number,
  "new_totusati": Number
};