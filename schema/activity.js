/**
 * created by matteodellea on 11/03/16.
 */

"use strict";

const ActivityAttachment = require('./secondary/activity-attachment');
const ActivityParty = require('./secondary/activity-party');
const Annotation = require('./secondary/annotation');

module.exports = {
  activityid: String,
  activitytypecode: Number,
  actualdurationminutes: Number,
  actualend: String,
  actualstart: String,
  description: String,
  ownerid: String,
  owneridname: String,
  owninguser: String,
  prioritycode: Number,
  regardingobjectid: String,
  regardingobjectidname: String,
  regardingobjecttypecode: Number,
  scheduleddurationminutes: Number,
  scheduledend: String,
  scheduledstart: String,
  statecode: Number,
  statuscode: Number,
  subject: String,
  /*** appointment ***/
  isalldayevent: Boolean,
  location: String,
  new_activitytype: Number,
  new_activitytypefollowup: Number,
  new_contactactivity: Number,
  new_followupstartingdate: String,
  new_openfollowup: Boolean,
  new_sourcecontact: Number,
  /*** vehicledelivery ***/
  /** email **/
  messageid: String,
  mimetype: String,
  /*** task ***/
  /*** letter ***/
  address: String,
  /*** phonecall ***/
  phonenumber: String,
  /*** fax ***/
  coverpagename: String,
  faxnumber: String,
  /*** emailvision ***/
  new_channel: Number,
  /*** sms ***/
  /*** telemarketing ***/
  /*** testdrive ***/
  new_checkindate: String,
  new_checkoutdate: String,
  new_location: String,
  new_model: String,
  new_modelname: String,
  new_numberofvehicle: String,
  new_tipology: Number,
  new_workingconditions: String,
  new_workingconditionsname: String,
  sender: [ActivityParty],
  torecipient: [ActivityParty],
  cc: [ActivityParty],
  bcc: [ActivityParty],
  requiredattendee: [ActivityParty],
  optionalattendee: [ActivityParty],
  organizer: [ActivityParty],
  attachments: [ActivityAttachment],
  notes: [Annotation]
};