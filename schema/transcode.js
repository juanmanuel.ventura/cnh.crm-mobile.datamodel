/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  code: String,
  description: String,
  new_tabellacaresaidname: String,
  new_trascodificaid: String
};