/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  "account": require("./account"),
  "activity": require("./activity"),
  "contact": require("./contact"),
  "lead": require("./lead"),
  "opportunity": require("./opportunity"),
  "transcode": require("./transcode"),
  "vehicle": require("./vehicle")
};