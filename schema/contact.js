/**
 * Created by matteodellea on 11/03/16.
 */

"use strict";

const CustomerAddress = require("./secondary/customer-address");

module.exports = {
  accountid: String,
  accountidname: String,
  accountrolecode: String,
  address1_city: String,
  address1_country: String,
  address1_latitude: Number,
  address1_line1: String,
  address1_longitude: Number,
  address1_postalcode: String,
  address1_stateorprovince: String,
  contactid: String,
  customeraddress: [CustomerAddress],
  description: String,
  donotbulkemail: Boolean,
  donotemail: Boolean,
  donotphone: Boolean,
  donotpostalmail: Boolean,
  donotsendmm: Boolean,
  emailaddress1: String,
  firstname: String,
  lastname: String,
  mobilephone: String,
  new_halmet: String,
  new_normalizationstate: Number,
  statecode: Number,
  telephone1: String,
  telephone2: String
};