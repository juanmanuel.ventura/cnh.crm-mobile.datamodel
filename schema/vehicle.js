/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

const Account = require("./account");

module.exports = {
  ivc_aciprice: String,
  ivc_brandtext: String,
  ivc_carfirstregdate: String,
  ivc_carscrapdate: String,
  ivc_commercialsectoridname: String,
  ivc_comuneutilizzatore: String,
  ivc_emissionidname: String,
  ivc_familyarrangementidname: String,
  ivc_isimported: Boolean,
  ivc_lastregdate: String,
  ivc_leasingenddate: String,
  ivc_modeltext: String,
  ivc_new_productid: String,
  ivc_passo: Number,
  ivc_producttypeidname: String,
  ivc_provincia: String,
  ivc_registrationarrangementidname: String,
  ivc_targa: String,
  ivc_usetypeidname: String,
  ivc_vehicleid: String,
  ivc_warrantystartdate: String,
  ownerid: String,
  owneridname: String,
  relation_data: [Account]
};