/**
 * Created by matteodellea on 15/03/16.
 */

"use strict";

const parser = require("../xml-parser/index");

const Assignee = {
  "ivc_accountid": String,
  "ivc_customerassignmentid": String,
  "ivc_salesmanid": String,
  "ivc_salesmanidname": String,
  "ownerid": String,
  "owneridname": String,
  "ivc_accountidname": String,
  "ivc_hc": Boolean,
  "ivc_hsc": Boolean,
  "ivc_hst": Boolean,
  "ivc_l0": Boolean,
  "ivc_l1c": Boolean,
  "ivc_l1f": Boolean,
  "ivc_l2f": Boolean,
  "ivc_l3": Boolean,
  "ivc_l3c": Boolean,
  "ivc_l3f": Boolean,
  "ivc_m1": Boolean,
  "ivc_m2": Boolean,
  "statecode": Number
};

const CustomerStatistic = {
  "ivc_firstpurchasedate": String,
  "new_tempomediodiriacquisto": Number,
  "new_indicedifedelta": Number
};

const BrandStatistic = {
  "new_tot": Number,
  "new_totl": Number,
  "new_totl1": Number,
  "new_totl1c": Number,
  "new_totl1f": Number,
  "new_totl2": Number,
  "new_totl2c": Number,
  "new_totl2f": Number,
  "new_totl3": Number,
  "new_totm": Number,
  "new_totm1": Number,
  "new_totm2": Number,
  "new_totnuovi": Number,
  "new_totp": Number,
  "new_totpc": Number,
  "new_totps": Number,
  "new_totpsc": Number,
  "new_totpst": Number,
  "new_totusati": Number,
  "new_brandid": String,
  "new_brandidname": String,
  "new_omogeneo": Number,
  "new_brandstatisticid": String,
  "assignee": [Assignee],
  "customerstatistic": [CustomerStatistic]
};

const schema = {
  brandstatistic: [BrandStatistic]
};

const xml = `
  <BrandStatistic new_tot="1" New_totL="1" New_totL1="0" New_totL1c="0" New_totL1f="0" New_totL2="1" New_totL2c="0" New_totL2f="1" New_totL3="0" New_totM="0" New_totM1="0" New_totM2="0" New_totNuovi="1" New_totP="0" New_totPC="0" New_totPS="0" New_totPSc="0" New_totPSt="0" New_totUsati="0" new_brandid="B475CD21-8215-4602-A108-5AEA5128F3A3" new_brandidName="PSA" New_omogeneo="0" New_brandstatisticid="CA94D7EB-2DD5-4090-9922-1430E40E0C11">
    <Assignee ivc_accountid="29853CD7-22CA-E311-A3DE-0050569466E5" ivc_customerassignmentId="F18BFE73-CA09-E511-9A33-0050569417A2" ivc_salesmanid="3C06B4D8-2605-E511-BDD7-0050569417A4" ivc_salesmanidName="CARRERI LUCA" OwnerId="3EDFE74E-619E-E211-B54B-0050569417A4" OwnerIdName="Generic apllication da rivalidare A005658" ivc_accountidName="ALBERINI MARTA" ivc_hc="1" ivc_hsc="1" ivc_hst="1" ivc_l0="0" ivc_l1c="0" ivc_l1f="0" ivc_l2f="0" ivc_l3="0" ivc_l3c="0" ivc_l3f="0" ivc_m1="1" ivc_m2="1" statecode="0"/>
    <Assignee ivc_accountid="29853CD7-22CA-E311-A3DE-0050569466E5" ivc_customerassignmentId="F28BFE73-CA09-E511-9A33-0050569417A2" ivc_salesmanid="3F4924A0-9A9E-4C88-829E-BCB44D09D641" ivc_salesmanidName="MAZZOLA GIANPAOLO" OwnerId="3EDFE74E-619E-E211-B54B-0050569417A4" OwnerIdName="Generic apllication da rivalidare A005658" ivc_accountidName="ALBERINI MARTA" ivc_hc="0" ivc_hsc="0" ivc_hst="0" ivc_l0="0" ivc_l1c="1" ivc_l1f="1" ivc_l2f="1" ivc_l3="1" ivc_l3c="0" ivc_l3f="0" ivc_m1="1" ivc_m2="1" statecode="0"/>
    <customerStatistic ivc_firstpurchasedate="2014-03-18T23:00:00" new_tempomediodiriacquisto="0.0000000000" new_indicedifedelta="100.0000000000"/>
  </BrandStatistic>
  <BrandStatistic new_tot="1" New_totL="1" New_totL1="0" New_totL1c="0" New_totL1f="0" New_totL2="1" New_totL2c="0" New_totL2f="1" New_totL3="0" New_totM="0" New_totM1="0" New_totM2="0" New_totNuovi="0" New_totP="0" New_totPC="0" New_totPS="0" New_totPSc="0" New_totPSt="0" New_totUsati="1" new_brandid="7E199E72-B4EA-4E96-BDEA-C481764C228D" new_brandidName="IVECO" New_omogeneo="0" New_brandstatisticid="D72C247A-E74F-4928-8C3D-197B6D49EF2D"/>
  <BrandStatistic new_tot="1" New_totL="1" New_totL1="0" New_totL1c="0" New_totL1f="0" New_totL2="1" New_totL2c="0" New_totL2f="1" New_totL3="0" New_totM="0" New_totM1="0" New_totM2="0" New_totNuovi="0" New_totP="0" New_totPC="0" New_totPS="0" New_totPSc="0" New_totPSt="0" New_totUsati="1" new_brandid="4594C120-93BC-4590-A9D7-5300BD37EEB5" new_brandidName="MERCEDES BENZ" New_omogeneo="0" New_brandstatisticid="7115598B-1E6F-4E95-AB15-4F59BF42555F"/>
  <BrandStatistic new_tot="1" New_totL="1" New_totL1="1" New_totL1c="0" New_totL1f="1" New_totL2="0" New_totL2c="0" New_totL2f="0" New_totL3="0" New_totM="0" New_totM1="0" New_totM2="0" New_totNuovi="0" New_totP="0" New_totPC="0" New_totPS="0" New_totPSc="0" New_totPSt="0" New_totUsati="1" new_brandid="ECFCDC75-199A-444E-BEAB-5550F1997040" new_brandidName="VOLKSWAGEN" New_omogeneo="0" New_brandstatisticid="3AB2ED3C-5D87-4D9B-A188-915951D81761"/>
  `;

parser(xml, function(err, ris) {
  console.log("noschema", JSON.stringify(ris));
});

parser(xml, schema, function(err, ris) {
  console.log("with schema", JSON.stringify(ris));
});

//console.log(JSON.stringify(result));