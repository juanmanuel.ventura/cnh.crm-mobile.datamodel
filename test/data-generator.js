/**
 * Created by matteodellea on 15/03/16.
 */

"use strict";

var generator = require("../index").dataGenerator;
var delimiter = '\t';
var line = `29853CD7-22CA-E311-A3DE-0050569466E5	ALBERINI MARTA	NULL	NULL	NULL	ALBERINI MARTA	NULL	MARCARIA	V OGLIO 1	NULL	46010	MANTOVA	NULL	0	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	LBRMRT60P53E922K	NULL	46010|MARCARIA	0	0	0	0	0	0	0	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	<Assignee ivc_accountid="29853CD7-22CA-E311-A3DE-0050569466E5" ivc_customerassignmentId="F18BFE73-CA09-E511-9A33-0050569417A2" ivc_salesmanid="3C06B4D8-2605-E511-BDD7-0050569417A4" ivc_salesmanidName="CARRERI LUCA" OwnerId="3EDFE74E-619E-E211-B54B-0050569417A4" OwnerIdName="Generic apllication da rivalidare A005658" ivc_accountidName="ALBERINI MARTA" ivc_hc="1" ivc_hsc="1" ivc_hst="1" ivc_l0="0" ivc_l1c="0" ivc_l1f="0" ivc_l2f="0" ivc_l3="0" ivc_l3c="0" ivc_l3f="0" ivc_m1="1" ivc_m2="1" statecode="0"/><Assignee ivc_accountid="29853CD7-22CA-E311-A3DE-0050569466E5" ivc_customerassignmentId="F28BFE73-CA09-E511-9A33-0050569417A2" ivc_salesmanid="3F4924A0-9A9E-4C88-829E-BCB44D09D641" ivc_salesmanidName="MAZZOLA GIANPAOLO" OwnerId="3EDFE74E-619E-E211-B54B-0050569417A4" OwnerIdName="Generic apllication da rivalidare A005658" ivc_accountidName="ALBERINI MARTA" ivc_hc="0" ivc_hsc="0" ivc_hst="0" ivc_l0="0" ivc_l1c="1" ivc_l1f="1" ivc_l2f="1" ivc_l3="1" ivc_l3c="0" ivc_l3f="0" ivc_m1="1" ivc_m2="1" statecode="0"/>	<BrandStatistic new_tot="0" New_totL="0" New_totL1="0" New_totL1c="0" New_totL1f="0" New_totL2="0" New_totL2c="0" New_totL2f="0" New_totL3="0" New_totM="0" New_totM1="0" New_totM2="0" New_totNuovi="0" New_totP="0" New_totPC="0" New_totPS="0" New_totPSc="0" New_totPSt="0" New_totUsati="0" new_brandid="7E199E72-B4EA-4E96-BDEA-C481764C228D" new_brandidName="IVECO" New_omogeneo="0" New_brandstatisticid="3EAB2B1D-70F4-4013-BC7E-149FD8EC7545"/>	<customerStatistic ivc_firstpurchasedate="2014-03-18T23:00:00" new_tempomediodiriacquisto="0.0000000000" new_indicedifedelta="100.0123000000"/>	NULL`;
var arrayLine = line.split(delimiter);

//console.log(arrayLine);


generator(arrayLine, "account", function (result) {
  console.log(JSON.stringify(result));
});