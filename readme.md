# CNH CRM-MOBILE DATAMODEL

## TSV data (also containing nested xmls) to object according to the appropriate schema
#### `dataGenerator(data: Array, entityName: String, cb: Function)`

## Parses object to appropriate keys types (according to the appropriate schema)
#### `typeConverter(data: Object, entityName: String)`