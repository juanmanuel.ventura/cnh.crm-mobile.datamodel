/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'new_trascodificaid',
    'code',
    'description',
    'new_tabellacaresaidname'
  ]
};