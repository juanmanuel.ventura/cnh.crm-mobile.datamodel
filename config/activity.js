/**
 * Created by matteodellea on 15/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'activityid',
    'ownerid',
    'actualstart',
    'activitytypecode',
    'statecode',
    'subject',
    'scheduledend',
    'owneridname',
    'prioritycode',
    'scheduleddurationminutes',
    'actualdurationminutes',
    'description',
    'regardingobjectid',
    'regardingobjectidname',
    'owninguser',
    'scheduledstart',
    'statuscode',
    'regardingobjecttypecode',
    'actualend',
    'appointment_isalldayevent',
    'appointment_location',
    'appointment_new_sourcecontact',
    'appointment_new_contactactivity',
    'appointment_new_activitytype',
    'appointment_new_activitytypefollowup',
    'appointment_new_followupstartingdate',
    'appointment_new_openfollowup',
    'vehicledelivery_new_activitytype',
    'vehicledelivery_new_activitytypefollowup',
    'vehicledelivery_new_contactactivity',
    'vehicledelivery_new_followupstartingdate',
    'vehicledelivery_new_location',
    'vehicledelivery_new_openfollowup',
    'vehicledelivery_new_sourcecontact',
    'email_mimetype',
    'email_sender',
    'email_torecipients',
    'email_messageid',
    'email_new_sourcecontact',
    'email_new_activitytype',
    'email_new_contactactivity',
    'task_new_activitytype',
    'task_new_contactactivity',
    'task_new_sourcecontact',
    'letter_address',
    'letter_new_sourcecontact',
    'letter_new_activitytype',
    'letter_new_contactactivity',
    'phonecall_phonenumber',
    'phonecall_new_activitytype',
    'phonecall_new_contactactivity',
    'phonecall_new_sourcecontact',
    'fax_coverpagename',
    'fax_faxnumber',
    'fax_new_contactactivity',
    'fax_new_activitytype',
    'fax_new_sourcecontact',
    'emailvision_new_activitytypefollowup',
    'emailvision_new_channel',
    'emailvision_new_contactactivity',
    'emailvision_new_followupstartingdate',
    'emailvision_new_openfollowup',
    'emailvision_new_sourcecontact',
    'emailvision_new_activitytype',
    'emailvision_new_openfollowup',
    'sms_new_activitytype',
    'sms_new_contactactivity',
    'sms_new_sourcecontact',
    'telemarketing_new_contactactivity',
    'telemarketing_new_sourcecontact',
    'telemarketing_new_activitytype',
    'testdrive_new_modelname',
    'testdrive_new_workingconditionsname',
    'testdrive_new_activitytype',
    'testdrive_new_activitytypefollowup',
    'testdrive_new_checkindate',
    'testdrive_new_checkoutdate',
    'testdrive_new_contactactivity',
    'testdrive_new_followupstartingdate',
    'testdrive_new_location',
    'testdrive_new_numberofvehicle',
    'testdrive_new_openfollowup',
    'testdrive_new_sourcecontact',
    'testdrive_new_tipology',
    'testdrive_new_workingconditions',
    'testdrive_new_model',
    {
      name: 'sender',
      type: 'xml',
      mapping: {
        'sender': 'sender'
      }
    },
    {
      name: 'torecipient',
      type: 'xml',
      mapping: {
        'torecipient': 'torecipient'
      }
    },
    {
      name: 'cc',
      type: 'xml',
      mapping: {
        'cc': 'cc'
      }
    },
    {
      name: 'bcc',
      type: 'xml',
      mapping: {
        'bcc': 'bcc'
      }
    },
    {
      name: 'requiredattendee',
      type: 'xml',
      mapping: {
        'requiredattendee': 'requiredattendee'
      }
    },
    {
      name: 'optionalattendee',
      type: 'xml',
      mapping: {
        'optionalattendee': 'optionalattendee'
      }
    },
    {
      name: 'organizer',
      type: 'xml',
      mapping: {
        'organizer': 'organizer'
      }
    },
    {
      name: 'attachments',
      type: 'xml',
      mapping: {
        'attachment': 'attachments'
      }
    },
    {
      name: 'notes',
      type: 'xml',
      mapping: {
        'note': 'notes'
      }
    }
  ],
  cleanup: function (d) {
    /*** CLEANUP SUBTYPES PREFIXES ***/
    var regex = /appointment_|vehicledelivery_|email_|task_|letter_|phonecall_|fax_|emailvision_|sms_|telemarketing_|testdrive_/g;
    Object.keys(d).forEach(function (key) {
      if (key.match(regex)) {
        var newKey = key.replace(regex, "");
        d[newKey] = d[key];
        delete d[key];
      }
    });
    /*** ADD START-DATE & END-DATE ***/ //TODO: add more control.. not always scheduledstart/end
    d.startDate = new Date(d.scheduledstart).getTime() || 0;
    d.endDate = new Date(d.scheduledend).getTime() || 0;
  }
};