/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'contactid',
    'accountid',
    'firstname',
    'lastname',
    'description',
    'accountidname',
    'accountrolecode',
    'telephone1',
    'mobilephone',
    'telephone2',
    'emailaddress1',
    'address1_line1',
    'new_halmet',
    'address1_postalcode',
    'address1_city',
    'address1_stateorprovince',
    'address1_country',
    'new_normalizationstate',
    'statecode',
    'donotsendmm',
    'donotphone',
    'donotemail',
    'donotbulkemail',
    'donotpostalmail',
    'address1_latitude',
    'address1_longitude',
    {
      name: 'customeraddress',
      type: 'xml',
      mapping: {
        "customeraddress": "customeraddress"
      }
    }
  ]
};