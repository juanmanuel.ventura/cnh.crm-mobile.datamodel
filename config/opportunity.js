/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'opportunityid',
    'createdon',
    'modifiedon',
    'statuscode',
    'statecode',
    'name',
    'customerid',
    'new_sourcecontact',
    'campaignid',
    'new_referencecontactsid',
    'ownerid',
    'owneridname',
    'new_opendate',
    'new_probability',
    'actualvalue',
    'actualclosedate',
    'new_competitorid',
    'new_probability',
    {
      name: 'quoteproduct',
      type: 'xml',
      mapping: {
        "quote": "quoteproduct",
        "leasing": "newleasing",
        "body": "newbody",
        "ivecobody": "new_ivecobody",
        "newtrade": "newtrade"
      }
    }
  ]
};