/**
 * Created by matteodellea on 15/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'leadid',
    'accountid',
    'contactid',
    'firstname',
    'lastname',
    'new_criticalcustomer',
    'ownerid',
    'owneridname',
    'companyname',
    'new_criticalreason',
    'new_interlocutorrole',
    'address1_telephone1',
    'mobilephone',
    'telephone3',
    'emailaddress1',
    'address1_line1',
    'new_hamlet',
    'new_type',
    'address1_postalcode',
    'address1_stateorprovince',
    'address1_city',
    'address1_country',
    'campaignid',
    'campaignidname',
    'new_campaignactivityid',
    'new_campaignactivityidname',
    'new_channel',
    'new_customernotes',
    'new_sezioneid',
    'new_gruppoid',
    'new_classeid',
    'confirminterest',
    'new_assigntodealerdate',
    'ivc_ldp',
    'ivc_settorecommerciale',
    'ivc_model',
    'new_productinformation',
    'new_usedinformation',
    'new_vatcode',
    'new_taxcode',
    'new_privacy',
    'donotphone',
    'donotemail',
    'donotbulkemail',
    'donotpostalmail'
  ]
};