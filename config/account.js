/**
 * Created by matteodellea on 14/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'accountid',
    'name',
    'description',
    'primarycontactid',
    'primarycontactidname',
    'new_ordered_name_account',
    'address1_primarycontactname',
    'address1_city',
    'accountcategorycode',
    'address1_telephone1',
    'address1_telephone3',
    'address1_telephone2',
    'emailaddress1',
    'address1_addresstypecode',
    'address1_line1',
    'new_frazione',
    'address1_postalcode',
    'address1_stateorprovince',
    'address1_country',
    'ivc_isblocked',
    'new_statonormalizzazione',
    'websiteurl',
    'masteraccountidname',
    'masterid',
    'new_sezioneid',
    'new_gruppoid',
    'new_classeid',
    'new_lastcontactdate',
    'new_nextcontactdate',
    'new_preferredworkshopid',
    'new_taxcode',
    'new_partitaiva',
    'ivc_assignmentresearchkeys',
    'statecode',
    'donotsendmm',
    'new_contact',
    'donotphone',
    'donotemail',
    'donotbulkemail',
    'donotpostalmail',
    'address1_latitude',
    'address1_longitude',
    {
      name: 'assignments',
      type: 'xml',
      mapping: {
        "assignee": "assignments"
      }
    },
    {
      name: 'brandstatistics',
      type: 'xml',
      mapping: {
        "brandstatistic": "brandstatistics"
      }
    },
    {
      name: 'customerstatistic',
      type: 'xml',
      mapping: {
        "customerstatistic": "customerstatistic"
      }
    },
    {
      name: 'customeraddress',
      type: 'xml',
      mapping: {
        "customeraddress": "customeraddress"
      }
    }
  ]
};