/**
 * Created by matteodellea on 15/03/16.
 */

"use strict";

module.exports = {
  fields: [
    'ivc_vehicleid',
    'ivc_brandtext',
    'ownerid',
    'owneridname',
    'ivc_modeltext',
    'ivc_commercialsectoridname',
    'ivc_carfirstregdate',
    'ivc_new_productid',
    'ivc_targa',
    'ivc_carscrapdate',
    'ivc_lastregdate',
    'ivc_warrantystartdate',
    'ivc_isimported',
    'ivc_emissionidname',
    'ivc_passo',
    'ivc_registrationarrangementidname',
    'ivc_familyarrangementidname',
    'ivc_producttypeidname',
    'ivc_usetypeidname',
    'ivc_aciprice',
    'ivc_leasingenddate',
    'ivc_comuneutilizzatore',
    'ivc_provincia',
    {
      name: 'relation_data',
      type: 'xml',
      mapping: {
        'relation': 'relation_data'
      }
    }
  ]
};